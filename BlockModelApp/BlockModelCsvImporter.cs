﻿using System.Globalization;
using System.IO;
using System.Linq;
using BlockModelApp.Entities;

namespace BlockModelApp
{
    public class BlockModelCsvImporter
    {

        public BlockModelCsvImporter(string csvPath, string csvSeparator, string xLengthName, string yLengthName, string zLengthName,
                                    string xCentreName, string yCentreName, string zCentreName)
        {
            CsvPath = csvPath;
            CsvSeparator = csvSeparator;
            
            XLengthName = xLengthName;
            XCentreName = xCentreName;

            YLengthName = yLengthName;
            YCentreName = yCentreName;

            ZLengthName = zLengthName;
            ZCentreName = zCentreName;

        }

        public string CsvPath { get; set; }
        public string CsvSeparator { get; set; }

        public string XLengthName { get; set; }
        public string YLengthName { get; set; }
        public string ZLengthName { get; set; }
        public string XCentreName { get; set; }
        public string YCentreName { get; set; }
        public string ZCentreName { get; set; }

        public BlockModel ImportCsvToBlockModel()
        {
            StreamReader sr = new StreamReader(CsvPath);
            BlockModel bm = new BlockModel();
            
            bm.BlockXSize = -99;
            bm.BlockYSize = -99;
            bm.BlockZSize = -99;

            string headerLine = sr.ReadLine();
            if (headerLine == null)
            {
                return null;
            }

            var variables = headerLine.Split(CsvSeparator);
            bm.Variables = variables.Select((s, index) => new { s, index }).ToDictionary(x => x.s, x => x.index);

            string line;
            while ((line = sr.ReadLine()) != null)
            {
                string[] values = line.Split(CsvSeparator);

                if (bm.BlockXSize == -99)
                {
                    bm.BlockXSize = double.Parse(values[bm.Variables[XLengthName]], CultureInfo.InvariantCulture);
                }
                if (bm.BlockYSize == -99)
                {
                    bm.BlockYSize = double.Parse(values[bm.Variables[YLengthName]], CultureInfo.InvariantCulture);
                }
                if (bm.BlockZSize == -99)
                {
                    bm.BlockZSize = double.Parse(values[bm.Variables[ZLengthName]], CultureInfo.InvariantCulture);
                }

                Block singleBlock = new Block(bm.Blocks.Count,
                                     double.Parse(values[bm.Variables[XCentreName]], CultureInfo.InvariantCulture),
                                     double.Parse(values[bm.Variables[YCentreName]], CultureInfo.InvariantCulture),
                                     double.Parse(values[bm.Variables[ZCentreName]], CultureInfo.InvariantCulture));
                foreach (var variable in bm.Variables)
                {
                    if(variable.Key == XCentreName ||
                       variable.Key == YCentreName ||
                       variable.Key == ZCentreName ||
                       variable.Key == XCentreName ||
                       variable.Key == YCentreName ||
                       variable.Key == ZCentreName)
                        continue;
                    
                    singleBlock.VariableValues.Add(variable.Value, double.Parse(values[variable.Value], CultureInfo.InvariantCulture));
                }
                bm.Blocks.Add(singleBlock);
            }

            double minX = bm.Blocks.Min(x => x.CentroidX);
            double minY = bm.Blocks.Min(x => x.CentroidY);
            double minZ = bm.Blocks.Min(x => x.CentroidZ);

            bm.Blocks.ForEach(block =>
            {
                block.IndexX = (int)((block.CentroidX - minX) / bm.BlockXSize);
                block.IndexY = (int)((block.CentroidY - minY) / bm.BlockYSize);
                block.IndexZ = (int)((block.CentroidZ - minZ) / bm.BlockZSize);
            });

            return bm;
        }
    }
}
