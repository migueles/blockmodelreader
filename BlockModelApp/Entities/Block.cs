﻿using System.Collections.Generic;

namespace BlockModelApp.Entities
{
    public class Block
    {
        public int BlockId { get; set; }
        public double CentroidX { get; set; }
        public double CentroidY { get; set; }
        public double CentroidZ { get; set; }

        public int IndexX { get; set; }
        public int IndexY { get; set; }
        public int IndexZ { get; set; }

        public int NeighborXUpId { get; set; }
        public int NeighborYUpId { get; set; }
        public int NeighborZUpId { get; set; }
        public int NeighborXDownId { get; set; }
        public int NeighborYDownId { get; set; }
        public int NeighborZDownId { get; set; }
        public List<int> NeighborsXYById { get; set; }
        public Dictionary<int, double> VariableValues { get; set; }
        
        public Block(int blockId, double centroidX, double centroidY, double centroidZ)
        {
            BlockId = blockId;
            CentroidX = centroidX;
            CentroidY = centroidY;
            CentroidZ = centroidZ;
            NeighborsXYById = new List<int>(4);
            VariableValues = new Dictionary<int, double>();
        }

        public override string ToString()
        {
            return "ID: " + BlockId + "   X: " + CentroidX + "   Y: " + CentroidY + "   Z: " + CentroidZ;
        }
    }
}
