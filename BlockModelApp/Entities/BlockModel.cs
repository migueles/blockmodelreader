﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlockModelApp.Entities
{
    public class BlockModel
    {
        public double BlockXSize { get; set; }
        public double BlockYSize { get; set; }
        public double BlockZSize { get; set; }

        public List<Block> Blocks { get; set; }
        public Dictionary<string, int> Variables { get; set; }
        public Dictionary<Tuple<int, int, int>, Block> BlocksInXYZ { get; set; }
        public Dictionary<int, Block> BlocksById { get; set; }
        
        public BlockModel()
        {
            Blocks = new List<Block>();
            Variables = new Dictionary<string, int>();
            BlocksInXYZ = new Dictionary<Tuple<int, int, int>, Block>();
            BlocksById = new Dictionary<int, Block>();
        }

        public void FillBlockModelStructs()
        {
            BlocksById = Blocks.ToDictionary(block => block.BlockId, block => block);
            BlocksInXYZ = Blocks.ToDictionary(block => Tuple.Create(block.IndexX, block.IndexY, block.IndexZ), block => block);

            Blocks.ForEach(block =>
            {
                AddNeighbors(block);
            });
        }
        
        private void AddNeighbors(Block referenceBlock)
        {
            // TODO: Use Index instead of absolute coordinates
            for (int i = referenceBlock.IndexX - 1; i <= referenceBlock.IndexX + 1; i += 2)
            {
                Block block = FindBlockByIndex(i, referenceBlock.IndexY, referenceBlock.IndexZ);
                if (block != null)
                {
                    referenceBlock.NeighborsXYById.Add(block.BlockId);
                    if (i < referenceBlock.IndexX)
                    {
                        referenceBlock.NeighborXDownId = block.BlockId;
                    }
                    else
                    {
                        referenceBlock.NeighborXUpId = block.BlockId;
                    }
                }
            }

            for (int j = referenceBlock.IndexY - 1; j <= referenceBlock.IndexY + 1; j += 2)
            {
                Block block = FindBlockByIndex(referenceBlock.IndexX, j, referenceBlock.IndexZ);
                if (block != null)
                {
                    referenceBlock.NeighborsXYById.Add(block.BlockId);
                    if (j < referenceBlock.IndexY)
                    {
                        referenceBlock.NeighborYDownId = block.BlockId;
                    }
                    else
                    {
                        referenceBlock.NeighborYUpId = block.BlockId;
                    }
                }
            }

            for (int k = referenceBlock.IndexZ - 1; k <= referenceBlock.IndexZ + 1; k += 2)
            {
                Block block = FindBlockByIndex(referenceBlock.IndexX, referenceBlock.IndexY, k);
                if (block != null)
                {
                    if (k < referenceBlock.IndexZ)
                    {
                        referenceBlock.NeighborZDownId = block.BlockId;
                    }
                    else
                    {
                        referenceBlock.NeighborZUpId = block.BlockId;
                    }
                }
            }
        }


        public Block FindBlockByIndex(int x, int y, int z)
        {
            Tuple<int, int, int> xyz = Tuple.Create(x, y, z);
            if (BlocksInXYZ.ContainsKey(xyz))
            {
                return BlocksInXYZ[xyz];
            }

            return null;
        }

        public Block FindBlockById(int blockId)
        {
            if (BlocksById.ContainsKey(blockId))
            {
                return BlocksById[blockId];
            }

            return null;
        }
    }
}
