﻿using System;

namespace BlockModelApp
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello Wosh!");
            Console.WriteLine("Starting reading BlockModel at:" + DateTime.Now);

            try
            {
                BlockModelCsvImporter importer = new BlockModelCsvImporter
                (
                    "../../../demo_blocks.csv",
                    ",",
                    "xlength",
                    "ylength",
                    "zlength",
                    "xcentre",
                    "ycentre",
                    "zcentre"
                );

                var bm = importer.ImportCsvToBlockModel();

                bm.FillBlockModelStructs();
                Console.WriteLine("Readed blocks: " + bm.Blocks.Count);
                string varNames = string.Join(",", bm.Variables.Keys);

                Console.WriteLine("Readed Variables: " + varNames);
                Console.WriteLine("Ending reading BlockModel at:" + DateTime.Now);
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: reading BlockModel");
                throw;
            }
        }
    }
}
